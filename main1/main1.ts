//------class------

// class cars{
//  carName:string
//  constructor(name:string){
//      this.carName=name
//  }
// show(){
//     console.log(`my car name is ${this.carName}`)
// }
// }
// let car= new cars("fiat");
// console.log(car.carName)
// car.show();

//--------exstends------
// class cars{
//     carName:string
//     constructor(name:string){
//         this.carName=name
//     }
//    show(){
//        console.log(`my car name is ${this.carName}`)
//    }
//    }
//    class model extends cars{
//        constructor(name:string){
//            super(name)

//        }
//        show_detail(){
//            console.log('Details are...');
//        }
//    }
//    let model2 = new model('mohave')
//   console.log( model2.carName);
//    model2.show();
//    model2.show_detail();

 class person{
    firstName :string;
    lastName : string;
    constructor(f:string,l:string){
        this.firstName= f;
        this.lastName= l;

    }
    fullname(){
        return (`${this.firstName}${this.lastName}`)
    }
}
class Teacher extends person{

}
class Student extends person{

}
let teacher = new Teacher('a','b')
let student = new Student('a','b')
function test<T extends person>(person:T):T{
return person
}
let ti =test(teacher)