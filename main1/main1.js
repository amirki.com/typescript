"use strict";
//------class------
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
// class cars{
//  carName:string
//  constructor(name:string){
//      this.carName=name
//  }
// show(){
//     console.log(`my car name is ${this.carName}`)
// }
// }
// let car= new cars("fiat");
// console.log(car.carName)
// car.show();
//--------exstends------
// class cars{
//     carName:string
//     constructor(name:string){
//         this.carName=name
//     }
//    show(){
//        console.log(`my car name is ${this.carName}`)
//    }
//    }
//    class model extends cars{
//        constructor(name:string){
//            super(name)
//        }
//        show_detail(){
//            console.log('Details are...');
//        }
//    }
//    let model2 = new model('mohave')
//   console.log( model2.carName);
//    model2.show();
//    model2.show_detail();
var person = /** @class */ (function () {
    function person(f, l) {
        this.firstName = f;
        this.lastName = l;
    }
    person.prototype.fullname = function () {
        return ("" + this.firstName + this.lastName);
    };
    return person;
}());
var Teacher = /** @class */ (function (_super) {
    __extends(Teacher, _super);
    function Teacher() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Teacher;
}(person));
var Student = /** @class */ (function (_super) {
    __extends(Student, _super);
    function Student() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Student;
}(person));
var teacher = new Teacher('a', 'b');
var student = new Student('a', 'b');
function test(person) {
    return person;
}
var ti = test(teacher);
